package regex

import "regexp"

func MatchGroups(regex *regexp.Regexp, subject string) map[string]string {
	groupList := regex.FindStringSubmatch(subject)
	if groupList == nil {
		return nil
	}
	names := regex.SubexpNames()
	groups := map[string]string{}
	for idx, name := range names {
		if name != "" && groupList[idx] != "" {
			groups[name] = groupList[idx]
		}
	}
	return groups
}

func MatchGroupsString(regex string, subject string) map[string]string {
	compiled := regexp.MustCompile(regex)
	return MatchGroups(compiled, subject)
}
