package regex

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMatchGroups(t *testing.T) {
	regex := `(?:(?:(?P<user>[^@:\s]+)@)?(?P<host>.+))`

	test0 := ""
	test1 := "foobar"
	test2 := "foo@bar"

	groups0 := MatchGroupsString(regex, test0)
	groups1 := MatchGroupsString(regex, test1)
	groups2 := MatchGroupsString(regex, test2)

	assert.Nil(t, groups0)

	assert.Equal(t, 1, len(groups1))
	value, ok := groups1["host"]
	assert.True(t, ok)
	assert.Equal(t, "foobar", value)
	value, ok = groups1["user"]
	assert.False(t, ok)

	assert.Equal(t, 2, len(groups2))
	value, ok = groups2["host"]
	assert.True(t, ok)
	assert.Equal(t, "bar", value)
	value, ok = groups2["user"]
	assert.True(t, ok)
	assert.Equal(t, "foo", value)
}
